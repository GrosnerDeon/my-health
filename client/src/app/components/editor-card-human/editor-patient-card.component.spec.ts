import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorPatientCardComponent } from './editor-patient-card.component';

describe('EditorPatientCardComponent', () => {
  let component: EditorPatientCardComponent;
  let fixture: ComponentFixture<EditorPatientCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorPatientCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorPatientCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
