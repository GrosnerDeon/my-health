import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Genders } from '../../../definitions/patient-card';

@Component({
  selector: 'app-parameter-patient-card',
  templateUrl: './parameter-patient-card.component.html',
  styleUrls: ['./parameter-patient-card.component.css'],
  animations: [
    trigger(
      'inOutAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ height: 0, opacity: 0 }),
            animate('1s ease-out', 
                    style({ height: 48, opacity: 1 }))
          ]
        ),
        transition(
          ':leave', 
          [
            style({ height: 48, opacity: 1 }),
            animate('1s ease-in', 
                    style({ height: 0, opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class ParameterPatientCardComponent implements OnInit {
  @Input() parameter: string | number | Genders;
  @Input() nameField: string;
  @Input() isSelector?: boolean;

  @Output() parameterChange = new EventEmitter<string | number | Genders>();
    
  constructor() {
  }

  onParameterChange(model: string | number | Genders){
    this.parameter = model;
    this.parameterChange.emit(model);
}

  ngOnInit(): void {
  }
}
