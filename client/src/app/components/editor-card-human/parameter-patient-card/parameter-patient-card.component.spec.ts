import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterPatientCardComponent } from './parameter-patient-card.component';

describe('ParameterPatientCardComponent', () => {
  let component: ParameterPatientCardComponent;
  let fixture: ComponentFixture<ParameterPatientCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParameterPatientCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterPatientCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
