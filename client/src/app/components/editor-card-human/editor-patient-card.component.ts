import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPatientCard, Genders, IPatientCardCreate } from '../../definitions/patient-card';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { PatientCardsService } from '../../services/patient-cards.service'
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-editor-patient-card',
  templateUrl: './editor-patient-card.component.html',
  styleUrls: ['./editor-patient-card.component.css'],
  animations: [
    trigger(
      'inAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ opacity: 0 }),
            animate('1s ease-out', 
                    style({ opacity: 1 }))
          ]
        )
      ]
    )
  ]
})
export class EditorPatientCardComponent implements OnInit {
  isEdit = false;
  patientCard: IPatientCard | IPatientCardCreate;
  isCreate: boolean;

  constructor(private patientCardsService: PatientCardsService,
    private navigationService: NavigationService) {
      if (this.navigationService.navigationConfiguration.isCreate) {
        this.patientCard = this.patientCardsService.getCreatePatient();
      } else {
        this.patientCard = this.patientCardsService.getEditPatient();
      }
      
  }

  ngOnInit(): void {
  }

}
