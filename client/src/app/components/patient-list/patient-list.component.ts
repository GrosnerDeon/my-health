import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PatientCardsService } from '../../services/patient-cards.service';
import { IPatientCard, Genders } from '../../definitions/patient-card';
import { NavigationService } from 'src/app/services/navigation.service';
import { PressureMeasurementService } from '../../services/pressure-measurement.service';
import { ACTIONS } from 'src/app/definitions/navigation';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
  patientCards: IPatientCard[];
  @Input() isList: boolean;
  @Output() isListChange = new EventEmitter<boolean>();

  constructor(private patientCardsService: PatientCardsService,
    private pressureMeasurementService: PressureMeasurementService,
    private navigationService: NavigationService) { }

  ngOnInit(): void {
    this.getPatientCards();
  }

  getPatientCards () {
    this.patientCardsService.getPatientCards().subscribe((patientCards: IPatientCard[]) => {
      this.patientCards = patientCards.map((patientCard: IPatientCard) => ({
        _id: patientCard._id,
        gender: patientCard.gender,
        firstName: patientCard.firstName,
        lastName: patientCard.lastName,
        age: patientCard.age,
        weight: patientCard.weight,
        height: patientCard.height
      }))
    });

    this.pressureMeasurementService.getPressureMeasurement().subscribe(() => {

    });
  }

  onIsListChange(model: boolean){
    this.isList = model;
    this.isListChange.emit(model);
    console.log(model);
}

  changeCrad (patientCard: IPatientCard) {
    this.navigationService.defineView({
      action: ACTIONS.EDIT,
      patientCard
    })
  }

  delete (id: number | string) {
    this.patientCardsService.deletePatientCard(id).subscribe(() => {
      this.getPatientCards();
    })
  }

  goPressureMeasurementList(patient: IPatientCard) {
    this.navigationService.defineView({
      action: ACTIONS.LIST,
      patientCard: patient
    })
  }
}
