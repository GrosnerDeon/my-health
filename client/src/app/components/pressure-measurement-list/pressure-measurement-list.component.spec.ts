import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PressureMeasurementListComponent } from './pressure-measurement-list.component';

describe('PressureMeasurementListComponent', () => {
  let component: PressureMeasurementListComponent;
  let fixture: ComponentFixture<PressureMeasurementListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PressureMeasurementListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PressureMeasurementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
