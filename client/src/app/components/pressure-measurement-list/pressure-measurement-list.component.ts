import { Component, OnInit } from '@angular/core';
import { IPressureMeasurement } from 'src/app/definitions/pressure-measurement';
import { NavigationService } from 'src/app/services/navigation.service';
import { PressureMeasurementService } from 'src/app/services/pressure-measurement.service';
import { ACTIONS } from '../../definitions/navigation'

@Component({
  selector: 'app-pressure-measurement-list',
  templateUrl: './pressure-measurement-list.component.html',
  styleUrls: ['./pressure-measurement-list.component.css']
})
export class PressureMeasurementListComponent implements OnInit {
  pressureMeasurementList: IPressureMeasurement[];

  constructor(private pressureMeasurementService: PressureMeasurementService,
    private navigationService: NavigationService) { }

  ngOnInit(): void {
    this.getPressureMeasurement();
  }

  removePressureMeasurement (pressureMeasurement: IPressureMeasurement) {
    this.pressureMeasurementService.removePressureMeasurement(pressureMeasurement).subscribe(() => {
      this.getPressureMeasurement();
    });
  }

  editPressureMeasurement (pressureMeasurent: IPressureMeasurement) {
    this.navigationService.defineView({
      action: ACTIONS.EDIT,
      pressureMeasurent
    })
  }

  getPressureMeasurement () {
    this.pressureMeasurementService
    .getPressureMeasurementByPatientId(this.pressureMeasurementService.getPatient()._id)
    .subscribe((pressureMeasurementList: IPressureMeasurement[]) => {
      this.pressureMeasurementList = pressureMeasurementList;
  });
  }

}
