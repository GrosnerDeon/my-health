import { Component, OnInit } from '@angular/core';
import { IPressureMeasurement, IPressureMeasurementCreate } from 'src/app/definitions/pressure-measurement';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { PressureMeasurementService } from 'src/app/services/pressure-measurement.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-pressure-measurement-create-view',
  templateUrl: './pressure-measurement-create-view.component.html',
  styleUrls: ['./pressure-measurement-create-view.component.css'],
  animations: [
    trigger(
      'inAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ opacity: 0 }),
            animate('1s ease-out', 
                    style({ opacity: 1 }))
          ]
        )
      ]
    )
  ]
})
export class PressureMeasurementCreateViewComponent implements OnInit {

  isEdit = false;
  pressureMeasurement: IPressureMeasurementCreate | IPressureMeasurement;
  isCreate: boolean;

  constructor(private pressureMeasurementService: PressureMeasurementService,
    private navigationService: NavigationService) {
      if (this.navigationService.navigationConfiguration.isCreate) {
        this.pressureMeasurement = this.pressureMeasurementService.getPressureMeasurementCreate();
      } else {
        this.pressureMeasurement = this.pressureMeasurementService.getPressureMeasurementEdit();
      }
  }

  ngOnInit(): void {
  }

}
