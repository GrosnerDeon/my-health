import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PressureMeasurementCreateViewComponent } from './pressure-measurement-create-view.component';

describe('PressureMeasurementCreateViewComponent', () => {
  let component: PressureMeasurementCreateViewComponent;
  let fixture: ComponentFixture<PressureMeasurementCreateViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PressureMeasurementCreateViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PressureMeasurementCreateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
