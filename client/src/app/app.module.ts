import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { EditorPatientCardComponent } from './components/editor-card-human/editor-patient-card.component';
import { ParameterPatientCardComponent } from './components/editor-card-human/parameter-patient-card/parameter-patient-card.component';
import { PatientListComponent } from './components/patient-list/patient-list.component';
import { HeaderComponent } from './header/header.component';
import { PressureMeasurementListComponent } from './components/pressure-measurement-list/pressure-measurement-list.component';
import { PressureMeasurementCreateViewComponent } from './components/pressure-measurement-create-view/pressure-measurement-create-view.component';

@NgModule({
  declarations: [
    AppComponent,
    EditorPatientCardComponent,
    ParameterPatientCardComponent,
    PatientListComponent,
    HeaderComponent,
    PressureMeasurementListComponent,
    PressureMeasurementCreateViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
