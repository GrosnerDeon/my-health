export interface IPressureMeasurement {
    _id: number | string;
    linkToPatient: number,
    systolic: number,
    diastolic: number,
    date: string,
    pulse: number
}

export interface IPressureMeasurementCreate {
    systolic: number,
    diastolic: number,
    pulse: number,
    date: string
}