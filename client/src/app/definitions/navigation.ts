import { IPatientCard, IPatientCardCreate } from "./patient-card";
import { IPressureMeasurement, IPressureMeasurementCreate } from "./pressure-measurement";

export enum VIEW {
    PATIENT_CREATE_EDIT_VIEW = 'PATIENT_CREATE_EDIT_VIEW',
    PATIENT_LIST_VIEW = 'PATIENT_LIST_VIEW',
    PRESSURE_MEASUREMENT_LIST_VIEW = 'PRESSURE_MEASUREMENT_LIST_VIEW',
    PRESSURE_MEASUREMENT_CREATE_EDIT_VIEW = 'PRESSURE_MEASUREMENT_CREATE_EDIT_VIEW'
}

export enum HEADER_BUTTONS {
    ADD = 'add',
    DONE = 'done',
    BACK = 'back'
}

export enum ACTIONS {
    CREATE = 'CREATE',
    EDIT = 'EDIT',
    CANCEL = 'CANCEL',
    LIST = 'LIST'
}

export interface INavigationConfiguration {
    isEdit: boolean;
    isCreate: boolean;
    view: VIEW;
    nameView(isCreate?: boolean, patientName?: string): string;
    headerButtons: HEADER_BUTTONS[];
}

export interface IConfigurationView {
    action: ACTIONS,
    patientCard?: IPatientCard,
    patientCardCreate?: IPatientCardCreate,
    pressureMeasurent?: IPressureMeasurement,
    pressureMeasurentCreate?: IPressureMeasurementCreate 
}

export const VIEWS: Map<VIEW,INavigationConfiguration> = new Map([
    [
        VIEW.PATIENT_LIST_VIEW,
        {
            isCreate: false,
            isEdit: false,
            view: VIEW.PATIENT_LIST_VIEW,
            nameView: () => 'Patient List',
            headerButtons: [HEADER_BUTTONS.ADD]
        }
    ],
    [
        VIEW.PATIENT_CREATE_EDIT_VIEW,
        {
            isCreate: true,
            isEdit: false,
            view: VIEW.PATIENT_CREATE_EDIT_VIEW,
            nameView: (isCreate: boolean, patientName: string): string => 
                isCreate ? 'Create New Patient' : `Edit ${patientName} patient`,
            headerButtons: [HEADER_BUTTONS.DONE, HEADER_BUTTONS.BACK]
        }
    ],
    [
        VIEW.PRESSURE_MEASUREMENT_LIST_VIEW,
        {
            isCreate: false,
            isEdit: false,
            view: VIEW.PRESSURE_MEASUREMENT_LIST_VIEW,
            nameView: () => 'Pressure Measurment List',
            headerButtons: [HEADER_BUTTONS.ADD, HEADER_BUTTONS.BACK]
        }
    ],
    [
        VIEW.PRESSURE_MEASUREMENT_CREATE_EDIT_VIEW,
        {
            isCreate: true,
            isEdit: false,
            view: VIEW.PRESSURE_MEASUREMENT_CREATE_EDIT_VIEW,
            nameView: (isCreate: boolean, patientName: string): string => 
                isCreate ? 
                'Create New Pressure Measurment' : 
                `Edit Pressure Measurment for patient ${patientName}`,
            headerButtons: [HEADER_BUTTONS.DONE, HEADER_BUTTONS.BACK]
        }
    ],
])