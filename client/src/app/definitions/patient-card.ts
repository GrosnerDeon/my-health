export enum Genders {
    MALE = 'MALE',
    FEMALE = 'FEMALE'
}

export interface IPatientCard {
    _id: number | string;
    gender: Genders;
    firstName: string;
    lastName: string;
    age: number;
    weight: number;
    height: number;
}

export interface IPatientCardCreate {
    gender: Genders;
    firstName: string;
    lastName: string;
    age: number;
    weight: number;
    height: number;
}