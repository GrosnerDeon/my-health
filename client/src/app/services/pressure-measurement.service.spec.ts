import { TestBed } from '@angular/core/testing';

import { PressureMeasurementService } from './pressure-measurement.service';

describe('PressureMeasurementService', () => {
  let service: PressureMeasurementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PressureMeasurementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
