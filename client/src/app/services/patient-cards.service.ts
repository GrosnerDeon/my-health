import { Injectable } from '@angular/core';
import { IPatientCard, Genders, IPatientCardCreate } from '../definitions/patient-card';
import { Observable, of, Subscriber } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatientCardsService {
  private url = 'api/heroes';
  patientCardEdit: IPatientCard;
  patientCardCreate: IPatientCardCreate;

  httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
     })
  };

  constructor(private http: HttpClient) { }

  setEditPatient (patientCard: IPatientCard): void {
    this.patientCardEdit = patientCard;
  }

  getEditPatient (): IPatientCard {
    return this.patientCardEdit;
  }

  setCreatePatient (patientCard: IPatientCardCreate): void {
    this.patientCardCreate = patientCard;
  }

  getCreatePatient (): IPatientCardCreate {
    return this.patientCardCreate;
  }

  getPatientCards (): Observable<IPatientCard[]> {
    return this.http.get<IPatientCard[]>('api/patientCard/');
  }

  savePatientCard (patientCardCreate: IPatientCardCreate): Observable<any> {
    return this.http.post('api/patientCard/create', patientCardCreate)
  }
  
  updatePatientCard (patientCard: IPatientCard): Observable<any> {
    return this.http.put('api/patientCard/update', patientCard);
  }

  deletePatientCard (patientCardId: number | string): Observable<any> {
    return this.http.delete(`api/patientCard/${patientCardId}`);
  }
}
