import { TestBed } from '@angular/core/testing';

import { PatientCardsService } from './patient-cards.service';

describe('PatientCardsService', () => {
  let service: PatientCardsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PatientCardsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
