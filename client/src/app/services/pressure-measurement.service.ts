import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { IPressureMeasurement, IPressureMeasurementCreate } from '../definitions/pressure-measurement'
import * as moment from 'moment';
import { IPatientCard } from '../definitions/patient-card';

@Injectable({
  providedIn: 'root'
})
export class PressureMeasurementService {
  patient: IPatientCard;
  pressureMeasurementCreate: IPressureMeasurementCreate;
  pressureMeasurementEdit: IPressureMeasurement;

  constructor(private http: HttpClient) { }

  setPatient (patient: IPatientCard): void {
    this.patient = patient;
  }

  getPatient (): IPatientCard {
    return this.patient;
  }

  setPressureMeasurementCreate (pressureMeasurementCreate: IPressureMeasurementCreate) {
    this.pressureMeasurementCreate = pressureMeasurementCreate;
  }

  getPressureMeasurementCreate (): IPressureMeasurementCreate {
    return this.pressureMeasurementCreate
  }

  setPressureMeasurementEdit (pressureMeasurementEdit: IPressureMeasurement) {
    this.pressureMeasurementEdit = pressureMeasurementEdit;
  }

  getPressureMeasurementEdit (): IPressureMeasurement {
    return this.pressureMeasurementEdit;
  }

  getPressureMeasurement (): Observable<IPressureMeasurement[]> {
    return this.http.get<IPressureMeasurement[]>('api/pressureMeasurement/');
  }

  getPressureMeasurementByPatientId (patientId: number | string): Observable<IPressureMeasurement[]> {
    return this.http.get<IPressureMeasurement[]>(`api/pressureMeasurement/${patientId}`);
  }

  savePressureMeasurementByPatientId (patientId: number | string, pressureMeasurementCreate: IPressureMeasurementCreate): Observable<any> {
    pressureMeasurementCreate.date = moment().format('DD/MM/YYYY HH:mm');
    
    return this.http.post(`api/pressureMeasurement/create/${patientId}`, pressureMeasurementCreate);
  }

  removePressureMeasurement (pressureMeasurement: IPressureMeasurement): Observable<any> {
    return this.http.delete(`api/pressureMeasurement/${pressureMeasurement._id}`);
  }

  updatePressureMeasurement (pressureMeasurement: IPressureMeasurement): Observable<any> {
    pressureMeasurement.date = moment().format('DD/MM/YYYY HH:mm');

    return this.http.put('api/pressureMeasurement/update', pressureMeasurement)
  }
}
