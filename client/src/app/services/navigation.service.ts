import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { INavigationConfiguration, VIEW, VIEWS, ACTIONS, IConfigurationView } from '../definitions/navigation';
import { IPatientCardCreate } from '../definitions/patient-card';
import { PatientCardsService } from '../services/patient-cards.service'; 
import { PressureMeasurementService } from '../services/pressure-measurement.service';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  navigationConfiguration: INavigationConfiguration;
  subjectNavigationConfiguration: Subject<INavigationConfiguration>;

  constructor(private patientCardsService: PatientCardsService,
    private pressureMeasurementService: PressureMeasurementService) { 
    this.navigationConfiguration = VIEWS.get(VIEW.PATIENT_LIST_VIEW);
    this.subjectNavigationConfiguration = new Subject<INavigationConfiguration>();
  }

  getSubjectNavigationConfiguration (): Subject<INavigationConfiguration> {
    return this.subjectNavigationConfiguration;
  }

  getNavigationConfiguration (): void {
    this.subjectNavigationConfiguration.next(this.navigationConfiguration);
  }

  navigateToView (view: VIEW, isEdit: boolean = false, isCreate: boolean = false) {
    this.navigationConfiguration = VIEWS.get(view);
    this.navigationConfiguration.isEdit = isEdit;
    this.navigationConfiguration.isCreate = isCreate;
    this.getNavigationConfiguration();
  }

  defineView (configurationView: IConfigurationView) {
    switch(this.navigationConfiguration.view) {
      case VIEW.PATIENT_LIST_VIEW:
        switch(configurationView.action) {
          case ACTIONS.CREATE:
            this.patientCardsService.setCreatePatient(configurationView.patientCardCreate);
            this.navigateToView(VIEW.PATIENT_CREATE_EDIT_VIEW, false, true);
            break;
          case ACTIONS.EDIT:
            this.patientCardsService.setEditPatient(configurationView.patientCard);
            this.navigateToView(VIEW.PATIENT_CREATE_EDIT_VIEW, true, false);
            break;
          case ACTIONS.LIST:
            this.pressureMeasurementService.setPatient(configurationView.patientCard);
            this.navigateToView(VIEW.PRESSURE_MEASUREMENT_LIST_VIEW, false, false);
            break;  
        }
        break;
      case VIEW.PATIENT_CREATE_EDIT_VIEW:
        switch(configurationView.action) {
          case ACTIONS.CREATE:
            this.patientCardsService.savePatientCard(configurationView.patientCardCreate).subscribe(() => {
                this.navigateToView(VIEW.PATIENT_LIST_VIEW);
            });
            break;
          case ACTIONS.EDIT:
            this.patientCardsService.updatePatientCard(configurationView.patientCard).subscribe(() => {
              this.navigateToView(VIEW.PATIENT_LIST_VIEW);
            });
            break;
          case ACTIONS.CANCEL:
            this.patientCardsService.setCreatePatient(configurationView.patientCardCreate);
            this.patientCardsService.setEditPatient(configurationView.patientCard);
            this.navigateToView(VIEW.PATIENT_LIST_VIEW);
            break;
        }
        break;
      case VIEW.PRESSURE_MEASUREMENT_LIST_VIEW:
        switch(configurationView.action) {
          case ACTIONS.CREATE:
            this.pressureMeasurementService.setPressureMeasurementCreate(configurationView.pressureMeasurentCreate);
            this.navigateToView(VIEW.PRESSURE_MEASUREMENT_CREATE_EDIT_VIEW, false, true);
            break;
          case ACTIONS.EDIT:
            this.pressureMeasurementService.setPressureMeasurementEdit(configurationView.pressureMeasurent);
            this.navigateToView(VIEW.PRESSURE_MEASUREMENT_CREATE_EDIT_VIEW, true, false);
            break;
          case ACTIONS.CANCEL:
            this.navigateToView(VIEW.PATIENT_LIST_VIEW);
            break;
        }
        break;
      case VIEW.PRESSURE_MEASUREMENT_CREATE_EDIT_VIEW:
        switch(configurationView.action) {
          case ACTIONS.CREATE:
            this.pressureMeasurementService.savePressureMeasurementByPatientId(
              this.pressureMeasurementService.getPatient()._id,
              this.pressureMeasurementService.getPressureMeasurementCreate()
            ).subscribe(() => {
              this.navigateToView(VIEW.PRESSURE_MEASUREMENT_LIST_VIEW);
            });
            break;
          case ACTIONS.EDIT:
            this.pressureMeasurementService.updatePressureMeasurement(
              this.pressureMeasurementService.getPressureMeasurementEdit()
            ).subscribe(() => {
              this.navigateToView(VIEW.PRESSURE_MEASUREMENT_LIST_VIEW);
            });
            break;
          case ACTIONS.CANCEL:
            this.navigateToView(VIEW.PRESSURE_MEASUREMENT_LIST_VIEW);
            break;
        }
        break;
    }
  }
}
