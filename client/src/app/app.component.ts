import { Component, OnDestroy } from '@angular/core';
import { NavigationService } from './services/navigation.service'
import { trigger, style, animate, transition } from '@angular/animations';
import { INavigationConfiguration, VIEW } from './definitions/navigation';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger(
      'isListAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ opacity: 0,  }),
            animate('1s ease-in-out', 
                    style({ opacity: 1 }))
          ]
        )
      ]
    )
  ]
})

export class AppComponent implements OnDestroy {
  view: VIEW;
  navigationConfigurationSubscribe: Subscription;
  
  constructor (private navigationService: NavigationService) {
    this.view = navigationService.navigationConfiguration.view;
    this.navigationConfigurationSubscribe = this.navigationService.getSubjectNavigationConfiguration()
      .subscribe((navigationConfiguration: INavigationConfiguration) => {
        this.view = navigationConfiguration.view;
      });
  }

  ngOnDestroy () {
    this.navigationConfigurationSubscribe.unsubscribe();
  }
}
