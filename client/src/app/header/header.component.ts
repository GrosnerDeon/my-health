import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { INavigationConfiguration, HEADER_BUTTONS, ACTIONS, VIEW } from '../definitions/navigation';
import { Genders } from '../definitions/patient-card';
import { NavigationService } from '../services/navigation.service';
import { PatientCardsService } from '../services/patient-cards.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAdd = false;
  isDone = false;
  isBack = false;
  nameView: string;
  navigationConfigurationSubscribe: Subscription;

  constructor(
    private navigationService: NavigationService,
    private patientCardsService: PatientCardsService
  ) { 
    this.init(navigationService.navigationConfiguration);
  }

  ngOnInit(): void {
    this.navigationConfigurationSubscribe = this.navigationService.getSubjectNavigationConfiguration()
      .subscribe((navigationConfiguration: INavigationConfiguration) => {
        this.init(navigationConfiguration);
    });
  }

  setTitle (navigationConfiguration: INavigationConfiguration): string {
    if (navigationConfiguration.view === VIEW.PATIENT_CREATE_EDIT_VIEW) {
      return `${this.patientCardsService.patientCardEdit.firstName} ${this.patientCardsService.patientCardEdit.lastName}`
    }

    return ''
  }

  init (navigationConfiguration: INavigationConfiguration) {
    this.nameView = navigationConfiguration.nameView(
      navigationConfiguration.isCreate,
      navigationConfiguration.isEdit ? 
      `${this.setTitle(navigationConfiguration)}` :
      '');
    this.isAdd = navigationConfiguration.headerButtons.includes(HEADER_BUTTONS.ADD);
    console.log(this.isAdd);
    this.isDone = navigationConfiguration.headerButtons.includes(HEADER_BUTTONS.DONE);
    this.isBack = navigationConfiguration.headerButtons.includes(HEADER_BUTTONS.BACK);
  }

  ngOnDestroy(): void {
    this.navigationConfigurationSubscribe.unsubscribe();
  }

  add() {
    this.navigationService.defineView({
      action: ACTIONS.CREATE,
      patientCardCreate: {
        lastName: '',
        firstName: '',
        age: 0,
        weight: 0,
        height: 0,
        gender: Genders.MALE
      },
      pressureMeasurentCreate: {
        systolic: 0,
        diastolic: 0,
        date: '',
        pulse: 0
      }
    });
  }

  done () {
    this.navigationService.defineView({
      action: this.navigationService.navigationConfiguration.isCreate ? ACTIONS.CREATE : ACTIONS.EDIT,
      patientCardCreate: this.patientCardsService.getCreatePatient(),
      patientCard: this.patientCardsService.getEditPatient()
    });
  }

  back () {
    this.navigationService.defineView({
      action: ACTIONS.CANCEL,
      patientCardCreate: {
        lastName: '',
        firstName: '',
        age: 0,
        weight: 0,
        height: 0,
        gender: Genders.MALE
      },
      pressureMeasurentCreate: {
        systolic: 0,
        diastolic: 0,
        date: '',
        pulse: 0
      }
    });
  }
}
