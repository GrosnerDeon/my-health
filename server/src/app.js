const express = require('express');
const config = require('config');
const mongoose = require('mongoose');

const app = express();
const PORT = config.get('port') || 5000;
const MONGO_URL = config.get('mongoUrl');

app.use(express.json({ extended: true }))
app.use('/api/patientCard', require('./routes/patientCardRoutes'))
app.use('/api/pressureMeasurement', require('./routes/pressureMeasurementRouters'))

async function startMongoDB () {
    try {
        await mongoose.connect(MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
    } catch (error) {
        console.log('Server error', error.message);
        process.exit();
    }
}

app.listen(PORT, () => {

});

startMongoDB();