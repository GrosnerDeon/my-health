const {Schema, model} = require('mongoose');

const schema = new Schema({
    gender: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    age: { type: Number },
    weight: { type: Number },
    height: { type: Number },
    minSYS: { type: Number },
    maxSYS: { type: Number },
    minDIA: { type: Number },
    maxDIA: { type: Number },
    minPUL: { type: Number },
    maxPUL: { type: Number }
});

module.exports = model('PatientCard', schema);