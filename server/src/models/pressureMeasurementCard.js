const {Schema, model} = require('mongoose');

const schema = new Schema({
    linkToPatient: { type: String },
    systolic: { type: Number },
    diastolic: { type: Number },
    pulse: { type: Number },
    date: { type: String }
});

module.exports = model('PressureMeasurementCard', schema);