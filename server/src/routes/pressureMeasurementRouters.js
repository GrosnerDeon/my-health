const {Router} = require('express');
const router = Router();

const PressureMeasurementCard = require('../models/pressureMeasurementCard');

router.get('/', async (req, res) => {
    try {
        const pressureMeasurements = await PressureMeasurementCard.find();

        res.json(pressureMeasurements);
    } catch (error) {
        res.status(500).json({ message: error });
    }
});

router.get('/:idPatient', async (req, res) => {
    try {
        console.log(req.params.idPatient);
        const pressureMeasurements = await PressureMeasurementCard
            .find({ linkToPatient: req.params.idPatient });

            console.log(pressureMeasurements);

        res.json(pressureMeasurements);
    } catch (error) {
        res.status(500).json({ message: error });
    }
});

router.post('/create/:idPatient', async (req, res) => {
    try {
        const { 
            systolic,
            diastolic,
            pulse,
            date } = req.body;

            const pressureMeasurement = new PressureMeasurementCard({
                linkToPatient: req.params.idPatient,
                systolic,
                diastolic,
                pulse,
                date
            })

            

            await pressureMeasurement.save();

            res.status(201).json({ message: 'Good' });
        
    } catch (error) {
        res.status(500).json({ message: 'Server Error' });
    }
})

router.put('/update', async (req, res) => {
    try {
        await PressureMeasurementCard.findByIdAndUpdate(req.body._id, req.body);

        res.status(201).json({ message: 'Good' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Server Error' });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await PressureMeasurementCard.findByIdAndRemove(req.params.id);

        res.status(201).json({ message: 'Good' });
    } catch (error) {
        res.status(500).json({ message: 'Server Error' })
    }
})

module.exports = router;