const {Router} = require('express');
const router = Router();
const PatientCard = require('../models/patientCard');
const config = require('config');
const bloodPressureRate = config.get('bloodPressureRate');

router.get('/', async (req, res) => {
    try {
        const patients = await PatientCard.find();
        res.json(patients);
    } catch (error) {
        res.status(500).json({ message: 'Server Error' });
    }
}); 

router.post('/create', async (req, res) => {
    try {
        const { 
            gender,
            firstName,
            lastName,
            age,
            weight,
            height } = req.body;

            let patientCard;

            const setBloodPressureFromUser = value => {
                patientCard = new PatientCard({
                    gender,
                    firstName,
                    lastName,
                    age,
                    weight,
                    height,
                    minSYS: bloodPressureRate[gender].age[value].sys.min,
                    maxSYS: bloodPressureRate[gender].age[value].sys.max,
                    minDIA: bloodPressureRate[gender].age[value].dia.min,
                    maxDIA: bloodPressureRate[gender].age[value].dia.max,
                    minPUL: bloodPressureRate[gender].age[value].pul.min,
                    maxPUL: bloodPressureRate[gender].age[value].pul.max
                })
            }

            if (age <= 20) {
                setBloodPressureFromUser(20);
            } else if (age > 20 && age <= 30) {
                setBloodPressureFromUser(30);
            } else if (age > 30 && age <= 40) {
                setBloodPressureFromUser(40);
            } else if (age > 40 && age <= 50) {
                setBloodPressureFromUser(50);
            } else if ((age > 50 && age <= 60) || age > 60) {
                setBloodPressureFromUser(60);
            }

            await patientCard.save();

            res.status(201).json({ message: 'Good' });
    } catch (error) {
        res.status(500).json({ message: 'Server Error' });
    }
})

router.put('/application', async (req, res) => {
    try {
        console.log(req.body);
        res.status(201).json({ message: 'Application data was saved to server' });
    } catch (error) {
        res.status(500).json({ message: `Application data wasn't saved to serve` });
    }
})

router.put('/update', async (req, res) => {
    try {
        await PatientCard.findByIdAndUpdate(req.body._id, req.body);

        res.status(201).json({ message: 'Good' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Server Error' });
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await PatientCard.findByIdAndRemove(req.params.id);

        res.status(201).json({ message: 'Good' });
    } catch (error) {
        res.status(500).json({ message: 'Server Error' })
    }
})

module.exports = router;